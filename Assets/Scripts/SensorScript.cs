﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorScript : MonoBehaviour
{
    public EspacoScript espacoAtivo;

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "V" && GameRules.isOver == false)
        {
            espacoAtivo = collision.GetComponent<EspacoScript>();
        }
    }

}
