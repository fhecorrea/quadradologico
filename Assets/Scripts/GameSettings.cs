using UnityEngine;
using UnityEngine.UI;


public class GameSettings : MonoBehaviour
{
    public AudioSource mainBgTrack;
    public GameObject settingsPanel, timerSetter;
    public Text muteButtonText;
    public Toggle enableSoundToggle, enableSFXToggle;
    protected GameObject gObjToHideOnSettingsPanelAppearing;
    protected bool bgTrackisPlaying, bgTrackIsEnabled, sfxIsEnabled, isGeneralSettings = false, isTimerEnabled = false, bgTrackIsPaused = false;
    protected string QUAD_LOG_ENABLED_BGSOUND = "_quadlog_enabled_music", QUAD_LOG_ENABLED_SFX = "_quadlog_enabled_sfx", QUAD_LOG_TIMER_SECS = "_quadlog_timer_secs";

    protected void SetInitialSettings()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        if (this.mainBgTrack == null)
        {
            this.mainBgTrack = GetComponent<AudioSource>();
        }
    }
    public void ToggleSettings()
    {
        if (!settingsPanel.activeSelf)
        {
            LoadPresetSettings();
        }
        settingsPanel.SetActive(!settingsPanel.activeSelf);
        gObjToHideOnSettingsPanelAppearing.SetActive(!gObjToHideOnSettingsPanelAppearing.activeSelf);
    }
    public void ToggleAudio()
    {
        this.bgTrackIsEnabled = !this.bgTrackIsEnabled;
        SetPlayerPrefsAsBool(QUAD_LOG_ENABLED_BGSOUND, this.bgTrackIsEnabled);
    }
    /* * * Parte copiada do MancalaBrasil/Assets/Scripts/GameSettings.cs * * */
    public void ToggleBgTrack(bool state)
    {
        this.bgTrackIsEnabled = !this.bgTrackIsEnabled;
        SetPlayerPrefsAsBool(QUAD_LOG_ENABLED_BGSOUND, this.bgTrackIsEnabled);
    }
    public void ToggleSFX(bool state)
    {
        this.sfxIsEnabled = !this.sfxIsEnabled;
        SetPlayerPrefsAsBool(QUAD_LOG_ENABLED_SFX, this.sfxIsEnabled);

    }
    public void SetPlayerPrefsAsBool(string playerPrefConst, bool value)
    {
        PlayerPrefs.SetInt(playerPrefConst, (value ? 1 : 0));
    }
    protected void ReloadAudioState()
    {
        bgTrackIsEnabled = this.BgTrackIsEnabled();
        sfxIsEnabled = this.SFXIsEnabled();
        //Debug.Log("Áudio habilitado? " + (this.bgTrackIsEnabled ? "Sim" : "Não") + "\nEfeitos habilitados? " + (this.sfxIsEnabled ? "Sim" : "Não") + "\nÁudio parado? " + (this.mainBgTrack.isPlaying ? "Sim" : "Não") + "\nMúsica tocando? " + (this.mainBgTrack.isPlaying ? "Sim" : "Não"));
        // Coloca o audio de fundo para tocar, caso esteja habilitado e não esteja tocando
        if (this.bgTrackIsEnabled)
        {
            if (!this.mainBgTrack.isPlaying)
                this.mainBgTrack.Play();
            muteButtonText.text = "Desligar música";
        }
        if (!this.bgTrackIsEnabled)
        {
            if (this.mainBgTrack.isPlaying)
                this.mainBgTrack.Stop();
            muteButtonText.text = "Ligar música";
        }
    }
    protected void LoadPresetSettings()
    {
        // https://docs.unity3d.com/2019.1/Documentation/ScriptReference/UI.Toggle.html
        this.enableSoundToggle.SetIsOnWithoutNotify(this.BgTrackIsEnabled());
        this.enableSFXToggle.SetIsOnWithoutNotify(this.SFXIsEnabled());
        // Se for uma configuração avançada do jogo ou seja, uma configuração
        // que o jogador não poderá editar no decorrer de uma partida...
        if (this.isGeneralSettings)
        {
            this.timerSetter.SetActive(true);
            //this.enableTimerToggle.SetIsOnWithoutNotify(this.IsTimerEnabled());
        }
        else
        {
            this.timerSetter.SetActive(false);
        }
    }
    protected void PauseBgMusic()
    {
        //Debug.Log("Is paused: " + this.bgTrackIsPaused);
        if (this.mainBgTrack.isPlaying && this.bgTrackIsEnabled)
        {
            this.mainBgTrack.Pause();
        }
        else if (!this.mainBgTrack.isPlaying && this.bgTrackIsEnabled)
        {
            this.mainBgTrack.Play();
        }
    }
    protected bool BgTrackIsEnabled()
    {
        return GetPlayerPrefsAsBool(QUAD_LOG_ENABLED_BGSOUND);
    }
    protected bool SFXIsEnabled()
    {
        return GetPlayerPrefsAsBool(QUAD_LOG_ENABLED_SFX);
    }
    protected int GetGameTimer()
    {
        if (!PlayerPrefs.HasKey(QUAD_LOG_TIMER_SECS))
            return 0;
        else
            return PlayerPrefs.GetInt(QUAD_LOG_TIMER_SECS);
    }
    protected bool GetPlayerPrefsAsBool(string playerPrefConst)
    {
        return PlayerPrefs.HasKey(playerPrefConst) && PlayerPrefs.GetInt(playerPrefConst) == 1;
    }
}