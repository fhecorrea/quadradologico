﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadranteScript : MonoBehaviour
{
    public GameMarkScript controller;
    public GameObject bgGameObject;
    public int numeroDoQuadrante;
    public float velocidade;
    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("Controlador").GetComponent<GameMarkScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(numeroDoQuadrante.ToString()))
        {
            StartCoroutine(Giro());
        }
    }

    public IEnumerator Giro()
    {
        if (GameRules.isOver == false)
        {
            for (int p = 0; p < 2; p += 1)
            {
                controller.playersPanels[p].SetActive(false);
                controller.playersObjects[p].SetActive(false);
            }
            controller.HideButtons();
            for (int i = 0; i < 90; i++)
            {
                yield return new WaitForSeconds(velocidade);
                transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z - GameRules.sentidoGiro);
                bgGameObject.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z - GameRules.sentidoGiro);
                foreach (Transform child in transform)
                {
                    if (child != bgGameObject)
                        child.transform.eulerAngles = new Vector3(0, 0, child.transform.eulerAngles.z + GameRules.sentidoGiro);

                }
            }
            // Gira todos os espaços filhos
            /*foreach (Transform child in transform)
            {
                for (int i = 0; i < 90; i++)
                {
                    //yield return new WaitForSeconds(velocidade / 100);
                    child.transform.eulerAngles = new Vector3(0, 0, child.transform.eulerAngles.z + GameRules.sentidoGiro);
                }
            }*/
            controller.playersPanels[GameRules.pTurn].SetActive(true);
            controller.playersObjects[GameRules.pTurn].SetActive(true);
            StartCoroutine(this.CheckGame());
            //
            //Debug.Log("checked sent");

        }
    }

    private IEnumerator CheckGame()
    {
        yield return new WaitForSeconds(0.5f);
        controller.CheckPlayersGame();
        controller.ShowButtons();
        GameRules.isReady = true;
    }
}
