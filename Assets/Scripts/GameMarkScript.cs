﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Text.RegularExpressions;


public class GameMarkScript : GameSettings
{
    public AudioClip[] avTracks = new AudioClip[4];
    public AudioSource[] dropSFXAudioSources = new AudioSource[2];
    public AudioSource[] eTExpressionSounds = new AudioSource[6];
    public AudioSource wellSucceedGameSound, failedGameSound, fireworksSound, childrenCheeringSound;
    public AudioSource jokerLaugh = new AudioSource();
    public Button pauseBtn, toggleBgBtn;
    public Color emptyFieldColor;
    public Color[] colCorDoQuadrante;
    public Color[] charsColors = new Color[4] { Color.magenta, /* Cor da Delta */
                                            Color.cyan, /* Cor da Infiniy */
                                            Color.red, /* Cor do PI */
                                            Color.green /* Cor do Vector */ };
    public float[] etRounds;
    public Image[] infoPanelPlayersChars = new Image[2];
    public GameObject bolinha, confirmPanel, eTCharacter, finalRoundPanel, fireWorks1, fireWorks2, fireWorks3, gameOverPanel, infoPlayersPanel, giroPanel, markedFieldIndicator, pausePanel, recordIndicator, rankingPanel, sFXConfirm, tabuleiro;
    public GameObject[] sensores, buttons, quadrantes, backgrounds, scenarios, playersPanels = new GameObject[2], selectedQuadIndicators = new GameObject[4], playersObjects = new GameObject[2];
    public RankingPanelScript rpsInstance;
    public Sprite defaultFieldSprite, jokeSprite, pieceSprite, emptyFieldSprite;
    public Sprite[] availableChars = new Sprite[4];
    public Sprite[] availableMiniChars = new Sprite[4];
    public Text txtCorDoQuadrante, continueBtnText, gameModeRankingText, goPanelWinnerNameText, goPanelWinnerPointsText, goPanelLoserNameText, goPanelLoserPointsText, goPanelTitleText, goPanelPanelIntroText, goPanelContinueBtnText;
    public Text[] playersInfoFields = new Text[2], infoPanelPlayer1PointsText = new Text[3], infoPanelPlayer2PointsText = new Text[3], infoPanelPlayersNames = new Text[2];
    public Text[][] rankFields = new Text[10][]; // nome, pon, data
    char[] playersGreatestSeqOrientation = new char[2];
    char IN_A_COLUMN = 'c', IN_A_ROW = 'r';
    GameObject tmpHiddenPanel;
    GameObject[] tmpCreatedMarks;
    int passarVez = 5, currentRound = 1, scoreToWin = 6, turnsBeforeJoker = 4, currentTurn, markedFieldIdx = -1, winnerPoints, playerWon, winnerPlayer, markedFields = 0, idxToMarkAgainst1stPlayer, idxToMarkForCPU;
    int[] playersGreatestSeq, playersGreatestSeqStartIdx, playersPlayedPieces, playersPlayedJokers/*, playersWinnerPoints*/;
    int[][] playersRoundsScores;
    string winnerName, winnerDate;
    private Text[][] infoPanelPlayersPoints = new Text[2][];
    // Start is called before the first frame update
    void Start()
    {
        //Screen.SetResolution(1920, 1080, true);
        //Debug.LogFormat("Resolução atual: {0}x{1}", Screen.currentResolution.width, Screen.currentResolution.height);
        this.SetInitialSettings();
        GameRules.pTurn = Random.Range(0, 1);
        this.mainBgTrack.clip = avTracks[Random.Range(0, 3)];
        //this.mainBgTrack.Play();
        //this.playersPanels[GameRules.pTurn].SetActive(true);
        //this.playersObjects[GameRules.pTurn].SetActive(true);
        // lista todos os sensores
        for (int i = 0; i < sensores.Length; i++)
        {
            sensores[i] = GameObject.Find("s (" + i.ToString() + ")");
        }
        // lista todos os botões
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i] = GameObject.Find("Button (" + i.ToString() + ")");
            buttons[i].GetComponent<ButtonScript>().buttonNumber = i;
        }
        if (GameRules.isAdvanced)
            SortEtAppearings();
        //this.random = new System.Random();
        this.playersPlayedPieces = new int[2] { 0, 0 };
        this.playersPlayedJokers = new int[2] { 0, 0 };
        CleanPlayersRoundsScores();
        this.playersGreatestSeq = new int[2] { 0, 0 };
        this.playersGreatestSeqStartIdx = new int[2] { -1, -1 };
        float tabDist = 5.2f;
        float yTab = 0.8f;
        for (int p = 0; p < GameRules.playersChars.Length; p += 1)
        {
            Debug.Log("GameRules.playersChars[" + p.ToString() + "] after game starting: " + GameRules.playersChars[p].ToString());
            this.playersObjects[p].SetActive(p == GameRules.pTurn);
            this.playersPanels[p].SetActive(p == GameRules.pTurn);
            if (GameRules.playersChars[p] == -1)
            {
                bool ok = false;
                do
                {
                    GameRules.playersChars[p] = (int)System.Math.Floor(Random.Range(0, 4) * 1f);
                    if (p == 1)
                    {
                        if (GameRules.playersChars[p] != GameRules.playersChars[(p - 1)])
                            ok = true;
                    }
                    else
                        ok = true;
                }
                while (!ok);
            }
            SpriteRenderer sprPlayer = this.playersObjects[p].GetComponent<SpriteRenderer>();
            sprPlayer.sprite = this.availableChars[GameRules.playersChars[p]];
            // Espelha o sprite para refletir o jogador olhando para o tabuleiro, se for o caso
            if ((p == 0 && (GameRules.playersChars[p] == 1 || GameRules.playersChars[p] == 3)) || (p == 1 && (GameRules.playersChars[p] == 0 || GameRules.playersChars[p] == 2)))
            {
                sprPlayer.flipX = true;
            }
            Debug.Log("P" + (p + 1) + " => " + GameRules.playersChars[p] + " = " + (GameRules.playersChars[p] == 3) + " | " + this.playersObjects[p].name);
            // Dá a distãncia dos personagens para o tabuleiro,
            // de modo que não fiquem muito distantes ou muito colados ao tab....
            if (GameRules.playersChars[p] == 1) // Infinit
            {
                tabDist = (p == 1 ? 7f : -7f);
                yTab = .3f;
            }
            else if (GameRules.playersChars[p] == 3) // Vector
            {
                tabDist = (p == 1 ? 6f : -6f);
                yTab = .3f;
            }
            else
            {
                tabDist = (p == 1 ? 5.2f : -5.2f);
            }
            this.playersObjects[p].transform.position = new Vector2(tabDist, yTab);
            // O nome e o personagem do jogador
            if (GameRules.playersNames[p] != null) // em caso de teste
                this.infoPanelPlayersNames[p].text = GameRules.playersNames[p];
            this.infoPanelPlayersChars[p].sprite = this.availableMiniChars[GameRules.playersChars[p]];
        }
        ClearInfoPlayers();
        // Insere os placares por round dentro da variável privada
        this.infoPanelPlayersPoints[0] = this.infoPanelPlayer1PointsText;
        this.infoPanelPlayersPoints[1] = this.infoPanelPlayer2PointsText;
        this.gObjToHideOnSettingsPanelAppearing = this.pausePanel;
        GameRules.quadranteAtivo = 0;
        GameRules.sentidoGiro = 1;
        GameRules.isOver = false;
        GameRules.isReady = !GameRules.isOver;
    }
    void ClearInfoPlayers()
    {
        for (int p = 0; p < GameRules.playersNames.Length; p += 1)
        {
            playersInfoFields[p].text = (GameRules.playersNames[p] != null ? GameRules.playersNames[p] : "Player #" + (p + 1));
        }
    }
    void SortEtAppearings()
    {
        // Define de antemão quais as rodadas que o ET fará suas jogadas
        if (this.etRounds.Length == 0)
        {
            this.etRounds = new float[] { 0f, 0f, 0f, 0f, 0f };
        }
        int nextRand = 8; //4 * (5 - this.currentRound); // múltiplos de 4
        int toNextRand = nextRand + 2;
        float lastEtAppear = 0f;
        for (int apEt = 0; apEt < this.etRounds.Length; apEt += 1)
        {
            float etApRound = 0f;
            do
            {
                // A primeira aparição do ET nunca vai ocorrer depois da 11º rodada
                etApRound = Random.Range(nextRand, (lastEtAppear != 0f ? (int)System.Math.Floor(lastEtAppear + 3f) : 11)) * 1f;
                // Randomiza a aparição para o player 1 e player 2...
                if (Random.Range(0, 3) % 2 == 0) // 0, 1, 2 e 3
                {
                    etApRound += 0.5f;
                }
            } while (etApRound - lastEtAppear <= 1);
            this.etRounds[apEt] = etApRound;
            lastEtAppear = this.etRounds[apEt];
            nextRand = toNextRand;
            toNextRand += 2;
        }
        //* APENAS PARA FINS DE DEBUG...
        string debbugText = "O ET aparecerá nas rodadas: ";
        for (int apEt = 0; apEt < this.etRounds.Length; apEt += 1)
        {
            debbugText += this.etRounds[apEt].ToString() + ", ";
        }
        //*/
    }
    int _CountPoints(int p)
    {
        if (GameRules.isAdvanced)
        {
            if (currentRound == 1) // regras avançadas normais
            {
                return ((this.playersPlayedPieces[p] - (this.playersGreatestSeq[p] - this.playersPlayedJokers[p])) * -10) + ((this.playersGreatestSeq[p] - this.playersPlayedJokers[p]) * 100) + (this.playersPlayedJokers[p] * 1000);
            }
            // 
            else if (currentRound == 2) // peças avulsas descontam -100
            {
                return ((this.playersPlayedPieces[p] - (this.playersGreatestSeq[p] - this.playersPlayedJokers[p])) * -100) + ((this.playersGreatestSeq[p] - this.playersPlayedJokers[p]) * 100) + (this.playersPlayedJokers[p] * 1000);
            }
            else if (currentRound == 3) // Sem desconto de peças avulsas
            {
                return ((this.playersGreatestSeq[p] - this.playersPlayedJokers[p]) * 100) + (this.playersPlayedJokers[p] * 1000);
            }
        }
        else // Modo clássico
        {
            return ((this.playersPlayedPieces[p] - (this.playersGreatestSeq[p] - this.playersPlayedJokers[p])) * -10) + ((this.playersGreatestSeq[p] - this.playersPlayedJokers[p]) * 100) + (this.playersPlayedJokers[p] * 1000);
        }
        return -1; // Erro!
    }
    /**
     * Gera a pontuação dos jogadores na fase em disputa, conforme
     * número de peças dispostas e respeitando as formas de contagem
     * na presente fase.
     */
    void CountPoints()
    {
        if (!GameRules.isOver)
        {
            for (int p = 0; p < 2; p += 1)
            {
                this.playersRoundsScores[p][currentRound - 1] = _CountPoints(p);
                Debug.Log("R: " + (currentRound - 1) + " / P: " + p + " / PP: " + this.playersPlayedPieces[p] + " / PW: " + this.playersGreatestSeq[p] + " / PJ: " + this.playersPlayedJokers[p] + " / Total: " + ((this.playersPlayedPieces[p] - (this.playersGreatestSeq[p] - this.playersPlayedJokers[p])) * -10) + ((this.playersGreatestSeq[p] - this.playersPlayedJokers[p]) * 100) + (this.playersPlayedJokers[p] * 1000));
            }
            Debug.Log("(A) Fase #" + currentRound.ToString() + ": P1=" + this.playersRoundsScores[0][currentRound - 1].ToString() + " P2=" + this.playersRoundsScores[1][currentRound - 1].ToString());
            if (currentRound >= backgrounds.Length || GameRules.isAdvanced == false)
            {
                continueBtnText.text = "Ver resultado final";
            }
            else
            {
                continueBtnText.text = "Ir para a fase #" + (currentRound + 1).ToString() + "...";
            }
            FinishRound();
        }
    }
    // Update is called once per frame
    void Update()
    {
        ReloadAudioState();
        //Debug.LogFormat("Resolução atual: {0}x{1}", Screen.currentResolution.width, Screen.currentResolution.height);
        if (!GameRules.isOver)
        {
            //CheckPlayersGame();
            //CalcPlayersScore();
            if (GameRules.isReady)
            {
                // Verifica se é a vez do ET e se for, joga por ele.
                if (GameRules.isAdvanced)
                {
                    for (int p = 0; p < etRounds.Length; p += 1)
                    {
                        //try
                        //{
                        if (etRounds[p] > 0f && GameRules.playedTurns == etRounds[p])
                        {
                            MarkForEt(p);
                        }
                    }
                }
                if (GameRules.is2ndPlayerAuto && GameRules.pTurn == 1 && markedFields < 64)
                {
                    MarkForCPU();
                }
            }
        }
        //CheckAudioState();
    }
    public void Mark(int button)
    {
        Mark(button, false);
    }
    public void Mark(int button, bool markedByEt = false)
    {
        bolinha = sensores[button].GetComponent<SensorScript>().espacoAtivo.gameObject;
        //Debug.Log("Btn(" + button + ") => " + bolinha.name + ": a1=" + bolinha.GetComponent<EspacoScript>().address[0] + ", a2=" + bolinha.GetComponent<EspacoScript>().address[1] + ", a3=" + bolinha.GetComponent<EspacoScript>().address[2] + ", a4=" + bolinha.GetComponent<EspacoScript>().address[3]);
        if (GameRules.isReady == true && bolinha.GetComponent<EspacoScript>().status == 0)
        {
            //this.playersPlayedPieces[GameRules.pTurn] += 1;
            Color playerColor = Color.grey; // auto CPU
            int playerStatus = 0;
            // Se houver um personagem escolhido (em caso de teste, pode não haver...)
            if (GameRules.playersChars[GameRules.pTurn] > -1)
            {
                playerColor = this.charsColors[GameRules.playersChars[GameRules.pTurn]];
            }
            // Atribui status, sons e outros artifícios de acordo com o personagem do jogador
            switch (GameRules.pTurn)
            {
                case 0:
                    playerStatus = 1;
                    break;
                case 1:
                    playerStatus = 2;
                    break;
            }
            // Marca o campo, incluindo o coringa
            if (IsJokerTime())
            {
                bolinha.GetComponent<EspacoScript>().status = 3;
                bolinha.GetComponent<SpriteRenderer>().sprite = jokeSprite;
                bolinha.GetComponent<SpriteRenderer>().transform.localScale = new Vector3(0.1f, 0.1f, 0);
                bolinha.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                //bolinha.GetComponent<EspacoScript>().address[3] = 1;
                //Debug.Log(bolinha.GetComponent<EspacoScript>().address);
                //jokersInGame.Add(bolinha.GetComponent<EspacoScript>().address);
                GameRules.playersPreJokerRemainingTurns[GameRules.pTurn] = 0;
                StartCoroutine(SFXConfirm());
                PlaySFxSound(jokerLaugh);
            }
            else // modo clássico ou não for a vez do coringa...
            {
                bolinha.GetComponent<SpriteRenderer>().sprite = pieceSprite;
                bolinha.transform.localScale = new Vector3(.6f, .6f, 0);
                bolinha.GetComponent<EspacoScript>().status = playerStatus;
                bolinha.GetComponent<SpriteRenderer>().color = playerColor;
                if (GameRules.isAdvanced)
                    GameRules.playersPreJokerRemainingTurns[GameRules.pTurn] += 1;
                StartCoroutine(SFXConfirm());
                //wasteP1 = wasteP1 + 1;
                this.playersPlayedPieces[GameRules.pTurn] += 1;
            }

            CheckPlayersGame();
            GameRules.playedTurns += 0.5f;
            GameRules.isReady = false;
            // Se for a jogada de um ET ou do CPU, seleciona e gira o quadrado automaticamente
            if (markedByEt || (GameRules.isAdvanced && GameRules.is2ndPlayerAuto && GameRules.pTurn == 1))
            {
                ChangeActiveQuadrante(Random.Range(0, 4));
                GameRules.sentidoGiro = (Random.Range(0, 4) % 2 == 0 ? 1 : -1);
                StartCoroutine(DelayGiro());
            }
            else if (GameRules.isAdvanced)
            {
                StartCoroutine(DelayGiroPanel());
            }
            else { StartCoroutine(DelayGiro()); }
            // Inverte a vez de jogar
            if (GameRules.pTurn == 0)
            {
                GameRules.pTurn = 1;
            }
            else if (GameRules.pTurn == 1)
            {
                GameRules.pTurn = 0;
            }
        }
    }
    void MarkForEt(int etTurnPos)
    {
        bool marked = false;
        int fieldToMark = 0;
        int currPlayer = GameRules.pTurn;
        eTCharacter.SetActive(true); // exibe o ET na tela...
        while (!marked)
        {
            fieldToMark = Random.Range(1, sensores.Length);
            //Debug.Log("Sensor:" + sensores[fieldToMark].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status.ToString());
            if (sensores[fieldToMark].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status == 0)
            {
                etRounds[etTurnPos] = 0;
                //Debug.Log("Marcando campo #" + fieldToMark.ToString() + " pelo ET");
                Mark(fieldToMark, true);
                marked = true;
                // Reproduz um dos sons do ET 
                // TODO: Colocar tudo dentro do ET e fazê-lo marcar por si só
                PlaySFxSound(eTExpressionSounds[Random.Range(0, 7)]);
            }
        }
        StartCoroutine(HideEt(currPlayer));
    }
    void MarkForCPU()
    {
        int i = GetIndexToMark();
        if (i != -1)
            Mark(i, false);
        return;
        bool marked = false;
        int fieldToMark = 0;
        while (!marked)
        {
            /*
            // TODO: Extrair uma possível jogada para o computador a partir da análise do jogo (AnalyzeGame)
            Debug.Log("playersGreatestSeq[0]: " + playersGreatestSeq[0].ToString() + " | _CountPoints(0): " + _CountPoints(1).ToString() + " | _CountPoints(1): " + _CountPoints(1).ToString());
            if (this.idxToMarkForCPU > -1)
                Debug.Log("this.idxToMarkForCPU - status on sensor(" + this.idxToMarkForCPU.ToString() + "): " + sensores[this.idxToMarkForCPU].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status.ToString());
            if (this.idxToMarkAgainst1stPlayer > -1)
                Debug.Log("this.idxToMarkAgainst1stPlayer - status on sensor(" + this.idxToMarkAgainst1stPlayer.ToString() + "): " + sensores[this.idxToMarkAgainst1stPlayer].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status.ToString());
            // Se o jogador-computador tiver uma sequência prestes a fechar 
            // jogo e o jogador adversário não estiver fechando jogo
            if (playersGreatestSeq[1] > 1 && (playersGreatestSeq[0] <= playersGreatestSeq[1]) && this.idxToMarkForCPU > -1 && sensores[this.idxToMarkForCPU].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status == 0)
            {
                // TODO: Estudar uma forma de distinguir lista horizontal da lista vertical e tentar marcar no fim dessa sequência para o próprio jogador...
                Debug.Log("A tentar fechar o próprio jogo... s(" + this.idxToMarkForCPU.ToString() + ")");
                Mark(this.idxToMarkForCPU, false);
                this.idxToMarkForCPU = -1;
                marked = true;
            }
            // Se a pontuação do jogador for maior que a pontuação do CPU 
            // e houver uma sequência prestes a se formar para o jogador #1,
            // tenta desfazer a sequência de jogo deste jogador.
            else if (playersGreatestSeq[0] > 3 && (_CountPoints(0) > _CountPoints(1)) && this.idxToMarkAgainst1stPlayer > -1 && sensores[this.idxToMarkAgainst1stPlayer].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status == 0)
            {
                // TODO: Estudar uma forma de distinguir lista horizontal da lista vertical e tentar marcar no fim dessa sequência do adversário...
                Debug.Log("A marcar próximo as marcações do jogador... s(" + this.idxToMarkAgainst1stPlayer.ToString() + ")");
                Mark(this.idxToMarkAgainst1stPlayer, false);
                this.idxToMarkAgainst1stPlayer = -1;
                marked = true;
            }
            else
            {
                */
            //while (!marked)
            //{
            if (this.markedFieldIdx == -1 && this.playersGreatestSeqStartIdx[1] == -1)
            {
                fieldToMark = Random.Range(0, sensores.Length);
                /* 
                * Se o campo a ser marcado for maior ou igual a 0 ou menor e igual a 63; 
                * E se os campos vizinhos dos lados esquerdo (-1) e direito (+1) e cima (-8) e baixo (+8)
                * Se houver um vizinho marcado, dá preferência a finalização do jogo, se não,
                * marca o primeiro campo desocupado ou ainda, desfaz o jogo do adversário.
                */
                int posLeftIdx = fieldToMark - 1 >= 0 ? fieldToMark - 1 : 0;
                int posRightIdx = fieldToMark + 1 < 64 ? fieldToMark + 1 : 63;
                int posBottomIdx = fieldToMark + 8 < 64 ? fieldToMark + 8 : 63;
                int posTopIdx = fieldToMark - 8 >= 0 ? fieldToMark - 8 : 0;
                EspacoScript eCurr = sensores[fieldToMark].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
                Debug.Log("Sorteado sensores(" + fieldToMark.ToString() + "), portador do status \"" + eCurr.status + "\" - mkFld: " + this.markedFieldIdx.ToString() + ", pgsstidx: " + this.playersGreatestSeqStartIdx[1].ToString());
                EspacoScript eLeft = sensores[posLeftIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
                EspacoScript eRight = sensores[posRightIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
                EspacoScript eTop = sensores[posTopIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
                EspacoScript eBottom = sensores[posBottomIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
                bool isJokerTime = IsJokerTime();
                if (((
                    ((((fieldToMark - 1) >= 0 && eLeft.status == 1) || ((fieldToMark + 1) < 64 && eRight.status == 1)) && this.playersGreatestSeqOrientation[0] == IN_A_ROW) ||
                    ((((fieldToMark - 8) >= 0 && eTop.status == 1) || ((fieldToMark + 8) < 64 && eBottom.status == 1)) && this.playersGreatestSeqOrientation[0] == IN_A_COLUMN)) ||
                    (!isJokerTime && this.playersGreatestSeq[0] > 3 && (
                        ((((fieldToMark - 1) >= 0 && eLeft.status == 2) || ((fieldToMark + 1) < 64 && eRight.status == 2)) && this.playersGreatestSeqOrientation[1] == IN_A_ROW) ||
                        ((((fieldToMark - 8) >= 0 && eTop.status == 2) || ((fieldToMark + 8) < 64 && eBottom.status == 2)) && this.playersGreatestSeqOrientation[1] == IN_A_COLUMN))
                    ) || (
                    ((fieldToMark - 1) >= 0 && eLeft.status == 3) ||
                    ((fieldToMark + 1) < 64 && eRight.status == 3) ||
                    ((fieldToMark - 8) >= 0 && eTop.status == 3) ||
                    ((fieldToMark + 8) < 64 && eBottom.status == 3)
                    ) || (markedFields < 3 || markedFields > 60)) && eCurr.status == 0)
                {
                    this.markedFieldIdx = fieldToMark;
                }
            }
            else
            {
                // Se a posição tiver sido ocupada pelo adversário, tenta 
                // ocupar a ponta inversa da sequência.
                // Caso contrário, tenta achar outra posição para reiniciar a jogada
                int t = -1;
                if (this.playersGreatestSeqOrientation[1] == IN_A_ROW)
                {
                    if (this.playersGreatestSeqStartIdx[1] > -1)
                    {
                        // Pega uma posição depois do último da sequencia
                        t = this.playersGreatestSeqStartIdx[1] + this.playersGreatestSeq[1];
                        Debug.Log("l: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") + len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                        if (t % 8 == 0 || t > 64 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                        {
                            // Acrescenta mais um, caso a posição selecionada esteja ocupada
                            t += 1;
                            Debug.Log("l+1: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") + len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                            if (t % 8 == 0 || t > 64 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                            {
                                // Tenta ao ponto inicial da sequencia
                                t = this.playersGreatestSeqStartIdx[1] - this.playersGreatestSeq[1];
                                Debug.Log("l: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") - len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                                if (t < 0 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                                {
                                    // Por fim, tenta voltar mais uma posição anterior ao início da sequência
                                    t -= 1;
                                    Debug.Log("l-1: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") - len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                                    if (t < 0 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                                    {
                                        this.markedFieldIdx = -1;
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        this.markedFieldIdx += 1;
                    }
                }
                else if (this.playersGreatestSeqOrientation[1] == IN_A_COLUMN)
                {
                    // Pega uma posição depois do último da sequencia
                    t = this.playersGreatestSeqStartIdx[1] + (8 * this.playersGreatestSeq[1]);
                    Debug.Log("c: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") + len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                    if (t % 8 == 0 || t > 64 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                    {
                        // Acrescenta mais um, caso a posição selecionada esteja ocupada
                        t += 8;
                        Debug.Log("c+1: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") + len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                        if (t % 8 == 0 || t > 64 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                        {
                            // Tenta ao ponto inicial da sequencia
                            t = this.playersGreatestSeqStartIdx[1] - (8 * this.playersGreatestSeq[1]);
                            Debug.Log("c: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") - len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                            if (t < 0 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                            {
                                // Por fim, tenta voltar mais uma posição anterior ao início da sequência
                                t -= 8;
                                Debug.Log("c-1: Seq. start (" + this.playersGreatestSeqStartIdx[1].ToString() + ") - len. seq (" + this.playersGreatestSeq[1].ToString() + ") = " + t.ToString());
                                if (t < 0 || sensores[t].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status != 0)
                                {
                                    this.markedFieldIdx = -1;
                                    continue;
                                }
                            }
                        }
                    }
                }
                // Seta o índice pesquisado para marcação a seguir
                this.markedFieldIdx = t;
            }
            // Finalmente, marca o campo
            if (this.markedFieldIdx > -1 && this.markedFieldIdx < 64)
            {
                int index = GetIndexToMark();
                Mark(index, false);
                marked = true;
                this.markedFieldIdx = index;
            }
            //} // while !marked #2
            //}
        } // while !marked #1
    }
    int GetIndexToMark()
    {
        int idxToMark = this.markedFieldIdx;
        if (idxToMark == -1)
            idxToMark = Random.Range(0, sensores.Length);
        int valToIncr = -1;
        if (this.playersGreatestSeqOrientation[1] == IN_A_ROW)
            valToIncr = 1;
        else if (this.playersGreatestSeqOrientation[1] == IN_A_COLUMN)
            valToIncr = 8;
        return AnalyzeIndexToMark(idxToMark, valToIncr);
    }
    /* 
    * Se o campo a ser marcado for maior ou igual a 0 ou menor e igual a 63; 
    * E se os campos vizinhos dos lados esquerdo (-1) e direito (+1) e cima (-8) e baixo (+8)
    * Se houver um vizinho marcado, dá preferência a finalização do jogo, se não,
    * marca o primeiro campo desocupado ou ainda, desfaz o jogo do adversário.
    */
    int AnalyzeIndexToMark(int idx, int valueToSum)
    {
        if (passarVez == 0)
        {
            passarVez = 5;
            return -1;
        }
        int posLeftIdx = idx - 1 >= 0 ? idx - 1 : 0;
        int posRightIdx = idx + 1 < 64 ? idx + 1 : 63;
        int posBottomIdx = idx + 8 < 64 ? idx + 8 : 63;
        int posTopIdx = idx - 8 >= 0 ? idx - 8 : 0;
        Debug.Log("Sorteado sensores(" + idx.ToString() + ")...");
        EspacoScript eCurr = sensores[idx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
        Debug.Log("... sensor com status \"" + eCurr.status + "\" - mkFld: " + this.markedFieldIdx.ToString() + ", pgsstidx: " + this.playersGreatestSeqStartIdx[1].ToString());
        EspacoScript eLeft = sensores[posLeftIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
        EspacoScript eRight = sensores[posRightIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
        EspacoScript eTop = sensores[posTopIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
        EspacoScript eBottom = sensores[posBottomIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
        bool isJokerTime = IsJokerTime();
        Debug.Log("posLeftIdx != idx? " + (posLeftIdx != idx).ToString() + "\nposRightIdx != idx? " + (posRightIdx != idx).ToString() + "\nposTopIdx != idx? " + (posTopIdx != idx).ToString() + "\nposBottomIdx != idx? " + (posBottomIdx != idx).ToString());
        Debug.Log("eLeft.status: " + eLeft.status.ToString() + "\neRight.status: " + eRight.status.ToString() + "\neTop.status: " + eTop.status.ToString() + "\neBottom.status: " + eBottom.status.ToString());
        Debug.Log("isJokerTime? " + isJokerTime.ToString() + "\nthis.playersGreatestSeq[0]: " + this.playersGreatestSeq[0].ToString() + "\nthis.playersGreatestSeqOrientation[0]: " + this.playersGreatestSeqOrientation[0].ToString() + "\nthis.playersGreatestSeqOrientation[1]: " + this.playersGreatestSeqOrientation[1].ToString());
        if ((((!isJokerTime && this.playersGreatestSeq[0] > 3 && this.playersGreatestSeq[0] > this.playersGreatestSeq[1] && (
            (((posLeftIdx != idx && eLeft.status == 1) || (posRightIdx != idx && eRight.status == 1)) && (this.playersGreatestSeq[1] < 3 || this.playersGreatestSeqOrientation[1] == IN_A_ROW)) ||
            (((posTopIdx != idx && eTop.status == 1) || (posBottomIdx != idx && eBottom.status == 1)) && (this.playersGreatestSeq[1] < 3 || this.playersGreatestSeqOrientation[1] == IN_A_COLUMN)))) ||
            ((((posLeftIdx != idx && eLeft.status == 2) || (posRightIdx != idx && eRight.status == 2)) && this.playersGreatestSeqOrientation[0] == IN_A_ROW) ||
            (((posTopIdx != idx && eTop.status == 2) || (posBottomIdx != idx && eBottom.status == 2)) && this.playersGreatestSeqOrientation[0] == IN_A_COLUMN)) ||
            ((posLeftIdx != idx && eLeft.status == 3) ||
            (posRightIdx != idx && eRight.status == 3) ||
            (posTopIdx != idx && eTop.status == 3) ||
            (posBottomIdx != idx && eBottom.status == 3)
            )) || (markedFields < 3 || markedFields > 60)) && eCurr.status == 0)
        {
            return idx;
        }
        else
        {
            idx += valueToSum;
            passarVez -= 1;
            return AnalyzeIndexToMark(idx, (idx % 8 == 0 ? -valueToSum : valueToSum));
        }
    }
    IEnumerator HideEt(int currPlayer)
    {
        yield return new WaitForSeconds(1);
        eTCharacter.GetComponent<Animator>().SetTrigger("disappear");
    }
    public void HideButtons()
    {
        for (int b = 0; b < buttons.Length; b += 1)
        {
            this.buttons[b].SetActive(false);
        }
    }
    public void ShowButtons()
    {
        for (int b = 0; b < buttons.Length; b += 1)
        {
            if (this.sensores[b].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status == 0)
            {
                this.buttons[b].SetActive(true);
            }
        }
    }
    IEnumerator DelayGiroPanel()
    {
        yield return new WaitForSeconds(1);
        if (GameRules.isOver == false)
        {
            ChangeActiveQuadrante(GameRules.quadranteAtivo);
            giroPanel.SetActive(true);
        }
    }

    IEnumerator DelayGiro()
    {
        //buttonGroup.SetActive(false);
        yield return new WaitForSeconds(1);
        quadrantes[GameRules.quadranteAtivo].SendMessage("Giro");
        //StartCoroutine(CheckGameAfterGiro());
        if (GameRules.isAdvanced == false)
        {
            if (GameRules.quadranteAtivo == 3)
            {
                GameRules.quadranteAtivo = 0;
            }
            else
            {
                GameRules.quadranteAtivo += 1;
            }
        }
        //buttonGroup.SetActive(true);
    }
    IEnumerator HideTable()
    {
        yield return new WaitForSeconds(1);
        tabuleiro.SetActive(false);
    }
    IEnumerator FireWorks()
    {
        int fireWorks = 3;
        fireWorks1.SetActive(false);
        fireWorks2.SetActive(false);
        fireWorks3.SetActive(false);
        fireWorks = Random.Range(0, 3);
        switch (fireWorks)
        {
            case 0:
                fireWorks1.SetActive(true);
                break;
            case 1:
                fireWorks2.SetActive(true);
                break;
            case 2:
                fireWorks3.SetActive(true);
                break;
            case 3:
                fireWorks1.SetActive(true);
                fireWorks2.SetActive(true);
                fireWorks3.SetActive(true);
                break;

        }
        yield return new WaitForSeconds(2);
        StartCoroutine(FireWorks());
    }
    IEnumerator DelayButtons()
    {
        yield return new WaitForSeconds(1);
        //buttonsPanel.SetActive(true);
        //SyncRecord(); TODO: Verificar recordes
    }

    bool IsJokerTime()
    {
        return GameRules.isAdvanced && GameRules.playersPreJokerRemainingTurns[GameRules.pTurn] >= this.turnsBeforeJoker;
    }

    public void CheckPlayersGame()
    {
        /**
         * Estuda as jogadas dos jogadores
         */
        //string[] rows = new string[8];
        //string[] cols = new string[8];
        string rowsAsStr = "", colsAsStr = "";
        int row, col = 0;
        int fieldStatus = 0;
        // TODO: Usar matrizes c#: int [,] colList = new int[8, 8];
        //int[] bestSequences = new int[] {0, 0};
        int[][] colList = new int[][] {
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 }
        };
        int[][] rowList = new int[][] {
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 },
            new int[] { 0, 0, 0, 0, 0, 0, 0, 0 }
        };
        //int[] topMarkPlayers = new int[] {0, 0}; // p1 e p2
        //int[] topMrkRow = new int[] { 0, 0 };
        //int[] topMrkCol = new int[] { 0, 0 };
        markedFields = 0;
        for (int c = 0; c < sensores.Length; c += 1)
        {
            row = (int)System.Math.Floor((float)(c / 8)); // arrumar sequencia: row=0, col=0, row=0, col=1, ...
            col = c % 8;
            try
            {
                fieldStatus = sensores[c].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status;
            }
            catch (System.NullReferenceException)
            {
                fieldStatus = 0;
            }
            //Debug.Log("Sensor: " + c + " / Row: " + row + " / Col: " + col + " / Status: " + fieldStatus);
            rowList[row][col] = fieldStatus;
            colList[col][row] = fieldStatus; // onde col=7 passa a ser row=1...
            if (fieldStatus != 0)
            {
                markedFields += 1;
            }
        }
        // Se não houverem campos vazios, declara encerrada a fase
        if (markedFields == 64)
        {
            FinishRound();
        }
        //string tmpRow = "";
        //string tmpCol = "";
        for (int p = 0; p < 8; p += 1)
        {
            for (int sp = 0; sp < 8; sp += 1)
            {
                //tmpRow += rowList[p][sp].ToString();
                //tmpCol += colList[p][sp].ToString();
                colsAsStr += colList[p][sp].ToString();
                rowsAsStr += rowList[p][sp].ToString();
                //Debug.Log("p: " + p + ", sp: " + sp + ", rowVal: " + rowList[p][sp]);
                //Debug.Log("p: " + p + ", sp: " + sp + ", colVal: " + colList[p][sp]);
            }
            colsAsStr += '\x20';
            rowsAsStr += '\x20';
            //rows[p] = tmpRow;
            //tmpRow = "";
            //cols[p] = tmpCol;
            //tmpCol = "";
        }
        //Debug.Log("Cols: " + string.Join(" | ", cols));
        //Debug.Log("Rows: " + string.Join(" | ", rows));
        //string[] summariedGame = new string[16];
        //System.Array.Copy(rows, summariedGame, rows.Length);
        //System.Array.Copy(cols, 0, summariedGame, rows.Length, cols.Length);
        //AnalizeGame(summariedGame);
        AnalizeGame(rowsAsStr, colsAsStr);
    }
    private void AnalizeGame(string rowsAsStr, string colsAsStr)
    {
        //int higherSeq = 0;
        //int higherSeqOwner = 0;
        //string rows = "00000000\n01123311\n00110000\n22322000\n00000000\n00000000";
        int[] charsSeq = new int[] { 0, 0 };
        string[] matrixAsStr = { rowsAsStr, colsAsStr };
        this.playersGreatestSeqOrientation = new char[] { '\x00', '\x00' };
        this.playersGreatestSeqStartIdx = new int[] { -1, -1 };
        for (int plyr = 1; plyr <= playersObjects.Length; plyr += 1)
        {
            for (int p = 0; p < matrixAsStr.Length; p += 1)
            {
                bool hasMatch = false;
                MatchCollection matchCollection = Regex.Matches(matrixAsStr[p], @"[" + plyr.ToString() + "3]{2,}");
                if (matchCollection.Count > 0)
                {
                    string matchStr = "";
                    foreach (Match m in matchCollection)
                    {
                        hasMatch = m.Success;
                        if (hasMatch)
                        {
                            //Debug.Log("num. groups: " + m.Groups.Count.ToString());
                            //greatestMatch
                            foreach (Group g in m.Groups)
                            {
                                if (g.Value.Length > charsSeq[plyr - 1])
                                {
                                    charsSeq[plyr - 1] = g.Value.Length;
                                    int playedJokers = 0;
                                    // Decompõe os valores para fins de contagem de pontos
                                    for (int nChar = 0; nChar < g.Value.Length; nChar += 1)
                                    {
                                        if (g.Value[nChar] == '3')
                                        {
                                            playedJokers += 1;
                                        }
                                    }
                                    this.playersPlayedJokers[plyr - 1] = playedJokers;
                                    matchStr = g.Value;
                                }
                            }
                        }
                        else
                        {
                            //Debug.Log("match.Success == " + hasMatch.ToString() + "!");
                        }
                    }
                    // Pega o próximo campo vazio para pre-marcar o jogo para o CPU
                    if (GameRules.is2ndPlayerAuto && matchStr.Length > 0)
                    {
                        int idxStr = matrixAsStr[p].IndexOf(matchStr);
                        int firstPos, toDiscount = 0;
                        //int fIdx = -1;
                        //this.idxToMarkAgainst1stPlayer = -1;
                        //this.idxToMarkForCPU = -1;
                        foreach (char ch in matrixAsStr[p].Substring(0, idxStr))
                        {
                            if (ch == '\x20')
                                toDiscount += 1;
                        }
                        // Primeira posição na sequência
                        firstPos = (int)System.Math.Floor((idxStr - toDiscount) / 8f);
                        // Acha as primeiras posições da sequência, caso linha ou caso coluna
                        if (p == 0)
                        {
                            this.playersGreatestSeqOrientation[plyr - 1] = this.IN_A_ROW;
                            this.playersGreatestSeqStartIdx[plyr - 1] = (((8 * firstPos) + ((idxStr - toDiscount) % 8)) + charsSeq[plyr - 1]) - 1;
                        }
                        else
                        {
                            this.playersGreatestSeqOrientation[plyr - 1] = this.IN_A_COLUMN;
                            this.playersGreatestSeqStartIdx[plyr - 1] = ((firstPos + (8 * ((idxStr - toDiscount) % 8))) + (charsSeq[plyr - 1] * 8)) - 1;
                        }

                        /*
                        if (p == 0)
                            fIdx = ((8 * firstPos) + ((idxStr - toDiscount) % 8)) + charsSeq[plyr - 1];
                        else
                            fIdx = (firstPos + (8 * ((idxStr - toDiscount) % 8))) + (charsSeq[plyr - 1] * 8);
                        // Tenta achar o próximo campo da sequência
                        int privateIdx = 0;
                        while (fIdx >= 0 && fIdx < 64)
                        {
                            if (p == 0)
                            {
                                if (((fIdx + 1) % 8) == 0)
                                {
                                    fIdx = firstPos - privateIdx++;
                                }
                                else
                                {
                                    fIdx += 1;
                                }
                            }
                            else
                            {
                                if ((fIdx + 8) > 63)
                                {
                                    fIdx = firstPos - (privateIdx++ * 8);
                                }
                                else
                                {
                                    fIdx += 8;
                                }
                            }
                            //if (sensores[fIdx].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status == 0)
                            //{
                            //    if (p == 0)
                            //    {
                            //        fIdx += privateIdx;
                            //    }
                            //}
                        }
                        // Acrescenta as variáveis para marcação do computador
                        if (plyr == 1)
                        {
                            this.idxToMarkAgainst1stPlayer = fIdx;
                        }
                        else
                        {
                            this.idxToMarkForCPU = fIdx;
                        }
                        */
                    }
                }
                else
                {
                    //Debug.Log("No match!");
                }
            }
        }
        ClearInfoPlayers();
        // Verifica o jogador que estiver na frente
        for (int c = 0; c < playersObjects.Length; c += 1)
        {
            this.playersGreatestSeq[c] = charsSeq[c];
        }
        int pl = -1;
        if (this.playersGreatestSeq[0] > this.playersGreatestSeq[1])
        {
            pl = 0;
        }
        else if (this.playersGreatestSeq[1] > this.playersGreatestSeq[0])
        {
            pl = 1;
        }
        //Debug.Log("Jogador na frente: " + pl.ToString());
        // Se o player que estiver a frente ter ganho o jogo
        if (pl != -1)
        {
            if (this.playersGreatestSeq[pl] >= scoreToWin)
            {
                char IS_ROW = 'r', IS_COL = 'c';
                char greatestMatchType = '\x00';
                string fullStr = "";
                string greatestMatch = "";

                winnerPlayer = pl;
                // Substituir variáveis duplicadas pxForTheWin pela variável 
                // abaixo e uma para indicar o vencedor da partida
                //winnerPlayerScore = charsSeq[c - 1];
                //this.playersWinnerPoints[c] = charsSeq[c];
                CountPoints();

                // TODO: Mover seção abaixo para o if abaixo, quando o jogador ter ganho a partida
                // ----- INICIO DO RECORTE -------
                // Pega o índice exato que começa a sequência, para depois tentar converter em posições
                // dos sensores e assim ter endereços exatas no tabuleiro
                for (int mIdx = 0; mIdx < 2; mIdx += 1)
                {
                    MatchCollection matchCollection = Regex.Matches(matrixAsStr[mIdx], @"[" + (pl + 1).ToString() + "3]{" + this.playersGreatestSeq[pl] + "}");
                    if (matchCollection.Count > 0)
                    {
                        // Pega o primeiro, caso haja mais que uma sequência 
                        // feita pelo jogador com o mesmo tamanho
                        if (matchCollection.Count > 1)
                        {
                            int idxToGet = 0, totalPoints = 0;
                            for (int mtchIdx = 0; mtchIdx < matchCollection.Count; mtchIdx += 1)
                            {
                                int points = 0;
                                for (int nc = 0; nc < matchCollection[mtchIdx].Groups[0].Value.Length; nc += 1)
                                {
                                    if (matchCollection[mtchIdx].Groups[0].Value[nc] == '3')
                                    {
                                        points += 10;
                                    }
                                    else
                                        points += 1;
                                }
                                // Se a sequência for maior, seleciona ela
                                if (points > totalPoints)
                                {
                                    totalPoints = points;
                                    idxToGet = mtchIdx;
                                }
                            }
                            greatestMatch = matchCollection[idxToGet].Groups[0].Value;
                        }
                        else
                        {
                            greatestMatch = matchCollection[0].Groups[0].Value;
                        }
                        fullStr = matrixAsStr[mIdx];
                        greatestMatchType = mIdx == 1 ? IS_COL : IS_ROW;
                        if (greatestMatchType == IS_COL)
                            Debug.Log("Colunas como string: " + colsAsStr);
                        if (greatestMatchType == IS_ROW)
                            Debug.Log("Linhas como string: " + rowsAsStr);
                        Debug.Log("greatestMatch: " + greatestMatch + " type: " + greatestMatchType.ToString());
                    }
                }
                int idxStr = fullStr.IndexOf(greatestMatch);
                int onSet, toDiscount = 0;
                int[] poss = new int[this.playersGreatestSeq[pl]];
                foreach (char ch in fullStr.Substring(0, idxStr))
                {
                    if (ch == '\x20')
                        toDiscount += 1;
                }
                Debug.Log("greatestMatchType=" + greatestMatchType.ToString());
                onSet = (int)System.Math.Floor((idxStr - toDiscount) / 8f);
                if (greatestMatchType == IS_ROW)
                    poss[0] = (8 * onSet) + ((idxStr - toDiscount) % 8);
                else
                    poss[0] = onSet + (8 * ((idxStr - toDiscount) % 8));
                for (int i = 1; i < this.playersGreatestSeq[pl]; i += 1)
                {
                    if (greatestMatchType == IS_ROW)
                        poss[i] = poss[i - 1] + 1;
                    else
                        poss[i] = poss[i - 1] + 8;
                }
                // TODO: Marcar se
                Debug.Log("Início a partir do sensor (" + (idxStr - toDiscount).ToString() + ") orig.(" + idxStr.ToString() + ") div. 8 (" + ((idxStr - toDiscount) / 8).ToString() + ") rest. div. 8 (" + ((idxStr - toDiscount) % 8).ToString() + ")");
                if (greatestMatchType == IS_ROW)
                    Debug.Log("Índice no sentido horizontal: início na linha (" + onSet.ToString() + "), colunas (" + string.Join(", ", poss) + ")");
                else
                    Debug.Log("Índice no sentido vertical: início na coluna (" + onSet.ToString() + "), linhas (" + string.Join(", ", poss) + ")");
                //Debug.Log("Índice da coluna vertical: coluna ({0}), linha ({1})", System.Math.Floor(idxStr/8f), (idxStr % 8f));
                Debug.Log("Tamanho: " + playersGreatestSeq[pl].ToString());
                // TODO: transferir esta parte para depois que o jogo houver terminado
                // criar um botão para ocultar/desocultar painel de fim de jogo, para fins de conferência
                MakeEnphasis(poss, (pl + 1));
                // ------- FIM DO RECORTE -------
            }
            else if (this.playersGreatestSeq[pl] > 4 && winnerPlayer != 0 && winnerPlayer != 1)
            {
                this.playersInfoFields[pl].text = "... opa! Preste atenção para ganhar a partida ...";
            }
        }
        //Debug.Log("----\nEstatísticas: \nMaior seq.: " + higherSeq.ToString() + "\nDono da maior seq.:" + higherSeqOwner.ToString() + "\nCampos marcados: " + markedFields.ToString() + "/64\n-----");
    }
    void MakeEnphasis(int[] indexesList, int playerStatus)
    {
        CleanEnphasis();
        //this.tmpCreatedMarks = new GameObject[indexesList.Length];
        this.tmpCreatedMarks = new GameObject[64];
        /*
        for (int idxOfindexes = 0; idxOfindexes < indexesList.Length; idxOfindexes += 1)
        {
            GameObject s = this.sensores[indexesList[idxOfindexes]];
            GameObject markedField = Instantiate(this.markedFieldIndicator, this.markedFieldIndicator.transform.parent.transform, false);
            markedField.transform.position = new Vector2(s.transform.position.x, s.transform.position.y);
            markedField.name = "marked_based_on_" + s.name;
            //Text t = markedField.GetComponent<Text>();
            //t.text = "";
            //t.color = 
            markedField.SetActive(true);
            this.tmpCreatedMarks[idxOfindexes] = markedField;
        }
        */
        for (int sIdx = 0; sIdx < 64; sIdx += 1)
        {
            GameObject s = this.sensores[sIdx];
            GameObject markedField = Instantiate(this.markedFieldIndicator, this.markedFieldIndicator.transform.parent.transform, false);
            markedField.transform.position = new Vector2(s.transform.position.x, s.transform.position.y);
            markedField.name = "marked_based_on_" + s.name;

            EspacoScript e = s.GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>();
            Text t = markedField.transform.GetChild(0).GetComponent<Text>();

            if (!indexesList.Contains(sIdx)) // se não estiver na lista de indices
            {
                if (e.status == playerStatus)
                {
                    markedField.GetComponent<Image>().color = Color.red;
                    t.color = Color.red;
                }
                else
                {
                    markedField.GetComponent<Image>().color = Color.black;
                }
            }
            else
            {
                t.color = Color.green;
            }

            if (GameRules.isAdvanced)
            {
                if (currentRound == 1) // regras avançadas normais
                {
                    if (e.status == playerStatus && !indexesList.Contains(sIdx))
                        t.text = "-10";
                    else if (e.status == playerStatus && indexesList.Contains(sIdx))
                        t.text = "+100";
                    else if (e.status == 3 && indexesList.Contains(sIdx))
                        t.text = "+1000";
                    else
                        t.text = "0";
                }
                // 
                else if (currentRound == 2) // peças avulsas descontam -100
                {
                    if (e.status == playerStatus && !indexesList.Contains(sIdx))
                        t.text = "-100";
                    else if (e.status == playerStatus && indexesList.Contains(sIdx))
                        t.text = "+100";
                    else if (e.status == 3 && indexesList.Contains(sIdx))
                        t.text = "+1000";
                    else
                        t.text = "0";
                }
                else if (currentRound == 3) // Sem desconto de peças avulsas
                {
                    if (e.status == playerStatus && indexesList.Contains(sIdx))
                        t.text = "+100";
                    else if (e.status == 3 && indexesList.Contains(sIdx))
                        t.text = "+1000";
                    else
                        t.text = "0";
                }
            }
            else // Modo clássico
            {
                if (e.status == playerStatus && !indexesList.Contains(sIdx))
                    t.text = "-10";
                else if (e.status == playerStatus && indexesList.Contains(sIdx))
                    t.text = "+100";
                else if (e.status == 3 && indexesList.Contains(sIdx))
                    t.text = "+1000";
                else
                    t.text = "0";
            }
            markedField.SetActive(true);
            this.tmpCreatedMarks[sIdx] = markedField;
        }
        Debug.Log("Campos " + string.Join(", ", indexesList) + " marcados!");
    }
    void CleanEnphasis()
    {
        if (this.tmpCreatedMarks != null && this.tmpCreatedMarks.Length > 0)
        {
            foreach (GameObject g in this.tmpCreatedMarks)
            {
                Destroy(g);
            }
        }
        this.tmpCreatedMarks = null;
    }
    public void ConfirmReturnMenu()
    {
        GameRules.quadranteAtivo = 0;
        GameRules.pTurn = 0;
        GameRules.isReady = true;
        GameRules.isOver = false;
        GameRules.isAdvanced = false;
        GameRules.playersPreJokerRemainingTurns = new int[] { 0, 0 };
        GameRules.playersNames = new string[] { null, null };
        GameRules.playersChars = new int[] { -1, -1 };
        //this.playersImages = new Image[2];
        //this.playersObjects = new GameObject[2];
        for (int pObjIdx = 0; pObjIdx < playersObjects.Length; pObjIdx += 1)
        {
            this.playersObjects[pObjIdx].GetComponent<SpriteRenderer>().flipX = false;
            this.playersObjects[pObjIdx].SetActive(false);
        }
        confirmPanel.SetActive(false);
        SceneManager.LoadScene("Menu");
    }
    public void ReturnMenu()
    {
        confirmPanel.SetActive(true);
    }
    public void CancelOp()
    {
        confirmPanel.SetActive(false);
    }
    public void RestartGame()
    {
        ResetGameState();
        CleanEnphasis();
        CleanPlayersRoundsScores();
        SceneManager.LoadScene("jogo");
        GameRules.isReady = true;
        GameRules.isOver = false;
    }
    void CleanPlayersRoundsScores()
    {
        this.playersRoundsScores = new int[2][] {
            new int[3] {0, 0, 0}, // round 1, round 2, round 3 of 1st player
            new int[3] {0, 0, 0} // round 1, round 2, round 3 of 2nd player
        };
    }
    void ResetGameState()
    {
        GameRules.quadranteAtivo = 0;
        GameRules.pTurn = 0;
        //GameRules.isAdvanced = false;
        GameRules.playersPreJokerRemainingTurns = new int[] { 0, 0 };
        this.playersPlayedPieces = new int[2] { 0, 0 };
        this.playersPlayedJokers = new int[2] { 0, 0 };
        //this.playersWinnerPoints = new int[2] { 0, 0 };
        this.playersGreatestSeq = new int[2] { 0, 0 };
        //GameRules.isOver = false;
        //GameRules.roundIsFinished = false;
        ChangeActiveQuadrante(3);
        ChangeActiveQuadrante(2);
        ChangeActiveQuadrante(1);
        ChangeActiveQuadrante(0);
        //GameRules.isReady = true;
        this.fireworksSound.Stop();
        this.wellSucceedGameSound.Stop();
        this.failedGameSound.Stop();
        this.childrenCheeringSound.Stop();
        this.mainBgTrack.Stop();
        this.mainBgTrack.clip = avTracks[Random.Range(0, 3)];
        //this.mainBgTrack.Play();
        // Limpa casas marcadas
        ClearMarks();
        CleanEnphasis();
    }
    public void ClearMarks()
    {
        //Debug.Log("Limpando campos marcados...");
        for (int i = 0; i < sensores.Length; i++)
        {
            //Debug.Log("Sensor #" + i.ToString());
            //Debug.Log(sensores[i].ToString());
            sensores[i].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<EspacoScript>().status = 0;
            sensores[i].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<SpriteRenderer>().sprite = emptyFieldSprite;
            sensores[i].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<SpriteRenderer>().color = emptyFieldColor;
            sensores[i].GetComponent<SensorScript>().espacoAtivo.gameObject.GetComponent<SpriteRenderer>().transform.localScale = new Vector3(0.065f, 0.065f, 0);
            // Resources/unity_builtin_extra/Knob
        }
        markedFields = 0;
    }
    public void PrepareNextRound()
    {
        //GameRules.isReady = false;
        //finalRoundPanel.SetActive(false);
        HideButtons();
        gameOverPanel.SetActive(false);
        infoPlayersPanel.SetActive(gameOverPanel.activeSelf);
        backgrounds[currentRound - 1].SetActive(false);
        if (scenarios[currentRound - 1] != null)
        {
            scenarios[currentRound - 1].SetActive(false);
        }
        if (scenarios[currentRound] != null)
        {
            scenarios[currentRound].SetActive(true);
        }
        backgrounds[currentRound].SetActive(true);
        // Decrementa em um o nº de rounds até aparição do coringa
        turnsBeforeJoker -= 1;
        // Incrementa em um o round atual
        currentRound += 1;
        // Zera as vezes jogadas na rodada (contador para aparição do ET)
        GameRules.playedTurns = 0f;
        // Reseta o estado do jogo, botões, campos, etc...
        ResetGameState();
        // Da a vez do jogador ganhador ao adversário
        if (winnerPlayer == 0) // Se era a vez de um, passa a ser do outro
        {
            GameRules.pTurn = 1;
        }
        else
        {
            GameRules.pTurn = 0;
        }
        // Sorteia novamente as rodadas para aparição dos ets
        SortEtAppearings();
        for (int p = 0; p < 2; p += 1)
        {
            if (GameRules.pTurn != p)
            {
                playersPanels[p].SetActive(false);
                playersObjects[p].SetActive(false);
            }
        }
        ShowButtons();
        ClearInfoPlayers();
        //tabuleiro.SetActive(true);
        GameRules.isOver = false;
        GameRules.isReady = true;
    }
    public void PauseGame()
    {
        Text pauseBtnTxt = pauseBtn.transform.GetChild(0).GetComponent<Text>();
        if (Time.timeScale == 1.0f)
        {
            this.PauseBgMusic();
            Time.timeScale = 0f;
            SetPlayersPoints();
            pauseBtnTxt.text = "Continuar jogo";
        }
        else
        {
            this.PauseBgMusic();
            Time.timeScale = 1f;
            pauseBtnTxt.text = "Pausar jogo";
        }
        this.pausePanel.SetActive(!this.pausePanel.activeSelf);
        this.infoPlayersPanel.SetActive(!this.infoPlayersPanel.activeSelf);
    }
    void SetPlayersPoints()
    {
        for (int p = 0; p < 2; p += 1)
        {
            for (int r = 0; r < 3; r += 1)
            {
                if (!GameRules.isOver && ((currentRound - 1) == r || this.playersRoundsScores[p][r] == 0))
                    this.infoPanelPlayersPoints[p][r].text = "0 pt.";
                else
                    this.infoPanelPlayersPoints[p][r].text = this.playersRoundsScores[p][r].ToString() + " pts.";
            }
        }
    }
    void FinishRound()
    {
        GameRules.isOver = true;
        GameRules.isReady = false;
        this.goPanelTitleText.text = "Fim da Fase <" + currentRound.ToString() + ">!";
        this.goPanelPanelIntroText.text = "O ganhador desta rodada foi...";
        if (currentRound < 3 && GameRules.isAdvanced)
        {
            this.goPanelContinueBtnText.text = "Ir p/ próxima fase";
        }
        else
        {
            this.goPanelContinueBtnText.text = "Ver resultado final";
        }
        for (int p = 0; p < 2; p += 1)
        {
            playersPanels[p].SetActive(true);
            playersObjects[p].SetActive(true);
        }
        // Se o P1 for melhor que P2
        if (this.playersRoundsScores[0][currentRound - 1] > this.playersRoundsScores[1][currentRound - 1])
        {
            goPanelWinnerNameText.text = "... " + GameRules.playersNames[0] + "!!!";
            goPanelWinnerPointsText.text = this.playersRoundsScores[0][currentRound - 1].ToString();
            this.playersRoundsScores[1][currentRound - 1] = 0;
            goPanelLoserNameText.text = "... " + GameRules.playersNames[1] + ".";
            goPanelLoserPointsText.text = this.playersRoundsScores[1][currentRound - 1].ToString();
            this.mainBgTrack.Stop();
            PlaySFxSound(childrenCheeringSound);
        }
        // Se P2 melhor que P1
        else if (this.playersRoundsScores[1][currentRound - 1] > this.playersRoundsScores[0][currentRound - 1])
        {
            goPanelWinnerNameText.text = "... " + GameRules.playersNames[1] + "!!!";
            goPanelWinnerPointsText.text = this.playersRoundsScores[1][currentRound - 1].ToString();
            this.playersRoundsScores[0][currentRound - 1] = 0;
            goPanelLoserNameText.text = "... " + GameRules.playersNames[0] + ".";
            goPanelLoserPointsText.text = this.playersRoundsScores[0][currentRound - 1].ToString();
            PlaySFxSound(failedGameSound);
        }
        else
        {
            goPanelWinnerNameText.text = "... EMPATE ...";
            goPanelWinnerPointsText.text = this.playersRoundsScores[0][currentRound - 1].ToString();
            this.playersRoundsScores[0][currentRound - 1] = this.playersRoundsScores[1][currentRound - 1] = 0;
            goPanelLoserNameText.text = "... empate.";
            goPanelLoserPointsText.text = this.playersRoundsScores[1][currentRound - 1].ToString();
        }
        gameOverPanel.SetActive(true);
        SetPlayersPoints();
        infoPlayersPanel.SetActive(gameOverPanel.activeSelf);
        //finalRoundPanel.SetActive(true);
        // Prepara e inicia o próximo round...
        //tabuleiro.SetActive(false);
    }
    public void EndGame()
    {
        int[] playersFinalScores = new int[] {
            this.playersRoundsScores[0][0] + this.playersRoundsScores[0][1] + this.playersRoundsScores[0][2],
            this.playersRoundsScores[1][0] + this.playersRoundsScores[1][1] + this.playersRoundsScores[1][2],
        };
        int winnerPlayerPoints = 0;
        string winnerPlayerNickname = "";

        this.goPanelTitleText.text = "Fim de jogo!";
        this.goPanelPanelIntroText.text = "O ganhador desta partida foi...";
        this.SetPlayersPoints();

        if (playersFinalScores[1] != playersFinalScores[0])
        {
            StartCoroutine(FireWorks());
            PlaySFxSound(fireworksSound);
        }
        this.mainBgTrack.Stop();
        PlaySFxSound(fireworksSound);
        PlaySFxSound(childrenCheeringSound);
        //StartCoroutine(DelayButtons());
        GameRules.isOver = true;
        //GameRules.isOver = true;
        for (int p = 0; p < 2; p += 1)
        {
            //playersFinalScores[p] += this.playersRoundsScores[p][currentRound - 1];
            playersPanels[p].SetActive(true);
            playersObjects[p].SetActive(true);
        }
        // Se o P1 for melhor que P2
        if (playersFinalScores[0] > playersFinalScores[1])
        {
            goPanelWinnerNameText.text = "..." + GameRules.playersNames[0] + "!!!";
            goPanelWinnerPointsText.text = playersFinalScores[0].ToString();
            goPanelLoserNameText.text = "..." + GameRules.playersNames[1];
            goPanelLoserPointsText.text = playersFinalScores[1].ToString();
            winnerPlayerPoints = playersFinalScores[0];
            winnerPlayerNickname = GameRules.playersNames[0];
        }
        // Se P2 melhor que P1
        else if (playersFinalScores[1] > playersFinalScores[0])
        {
            goPanelWinnerNameText.text = "..." + GameRules.playersNames[1] + "!!!";
            goPanelWinnerPointsText.text = playersFinalScores[1].ToString();
            goPanelLoserNameText.text = "..." + GameRules.playersNames[0];
            goPanelLoserPointsText.text = playersFinalScores[0].ToString();
            winnerPlayerPoints = playersFinalScores[1];
            winnerPlayerNickname = GameRules.playersNames[1];
        }
        // Jogo terminou empatado
        else
        {
            goPanelWinnerNameText.text = "... EMPATE...";
            goPanelWinnerPointsText.text = playersFinalScores[0].ToString();
            goPanelLoserNameText.text = "... empate.";
            goPanelLoserPointsText.text = playersFinalScores[1].ToString();
        }
        // Se os efeitos sonoros estiverem habiltados, faz o som de comemoração de vitória
        if (winnerPlayer == 1 && GameRules.is2ndPlayerAuto)
        {
            PlaySFxSound(failedGameSound);
        }
        else
        {
            PlaySFxSound(wellSucceedGameSound);
        }
        playersPanels[winnerPlayer].SetActive(true);
        playersObjects[winnerPlayer].SetActive(true);
        // Verifica se o jogador tem pontuação de ranking
        // e o notifica de sua inserção no mesmo
        if ((playersFinalScores[0] > playersFinalScores[1]) || ((playersFinalScores[1] > playersFinalScores[1]) && GameRules.is2ndPlayerAuto))
        {
            int rankPos;
            if (GameRules.isAdvanced)
                rankPos = rpsInstance.IsAbleToAdvancedGameModeLevel(winnerPlayerPoints);
            else
                rankPos = rpsInstance.IsAbleToClassicGameModeLevel(winnerPlayerPoints);
            if (rankPos > -1)
            {
                if (GameRules.isAdvanced)
                    rpsInstance.RegisterInAdvancedGameModeRank(rankPos, winnerPlayerNickname, winnerPlayerPoints);
                else
                    rpsInstance.RegisterInClassicGameModeRank(rankPos, winnerPlayerNickname, winnerPlayerPoints);
            }
        }
        gameOverPanel.SetActive(true);
        SetPlayersPoints();
        infoPlayersPanel.SetActive(gameOverPanel.activeSelf);
    }
    public IEnumerator SFXConfirm()
    {
        // check PlayerPrefs.GetInt(this.QUAD_LOG_ENABLED_AUDIO_STR) == 0 here too?
        //sFXConfirm.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        //sFXConfirm.SetActive(false);
        PlaySFxSound(dropSFXAudioSources[Random.Range(0, 2)]);
    }
    public void ChangeActiveQuadrante(int index)
    {
        selectedQuadIndicators[GameRules.quadranteAtivo].SetActive(false);
        switch (index)
        {
            case 0:
                txtCorDoQuadrante.text = "Escolhido o Vermelho";
                break;
            case 1:
                txtCorDoQuadrante.text = "Escolhido o Amarelo";
                break;
            case 2:
                txtCorDoQuadrante.text = "Escolhido o Verde";
                break;
            case 3:
                txtCorDoQuadrante.text = "Escolhido o Preto";
                break;
        }
        GameRules.quadranteAtivo = index;
        selectedQuadIndicators[GameRules.quadranteAtivo].SetActive(true);
    }

    public void GirarHorario()
    {
        GameRules.sentidoGiro = 1;
        StartCoroutine(DelayGiro());
        giroPanel.SetActive(false);
    }

    public void GirarAntiHorario()
    {
        GameRules.sentidoGiro = -1;
        StartCoroutine(DelayGiro());
        giroPanel.SetActive(false);
    }

    public void SelecionarQuadVermelho()
    {
        ChangeActiveQuadrante(0);
    }

    public void SelecionarQuadAmarelo()
    {
        ChangeActiveQuadrante(1);
    }

    public void SelecionarQuadPreto()
    {
        ChangeActiveQuadrante(3);
    }

    public void SelecionarQuadVerde()
    {
        ChangeActiveQuadrante(2);
    }
    public void Continue()
    {
        gameOverPanel.SetActive(false);
        infoPlayersPanel.SetActive(gameOverPanel.activeSelf);
        Debug.Log("currentRound: " + currentRound.ToString() + " | bg.Length: " + backgrounds.Length.ToString());
        if (GameRules.isAdvanced == false || currentRound >= backgrounds.Length)
        {
            this.EndGame();
        }
        else
        {
            this.PrepareNextRound();
        }
    }
    public void ToggleBackground()
    {
        if (pausePanel.activeSelf || finalRoundPanel.activeSelf)
        {
            if (pausePanel.activeSelf)
                tmpHiddenPanel = pausePanel;
            else if (finalRoundPanel.activeSelf)
                tmpHiddenPanel = finalRoundPanel;
            tmpHiddenPanel.SetActive(!tmpHiddenPanel.activeSelf);
            toggleBgBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Re-exibir painel";
        }
        else if (tmpHiddenPanel != null)
        {
            tmpHiddenPanel.SetActive(!tmpHiddenPanel.activeSelf);
            toggleBgBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Mostrar tabuleiro";
            tmpHiddenPanel = null;
        }
    }
    void PlaySFxSound(AudioSource a)
    {

        if (this.SFXIsEnabled())
        {
            a.Play();
        }
    }
}
