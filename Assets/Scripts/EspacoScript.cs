﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspacoScript : MonoBehaviour
{
    public int status;
    public int rotationState = 0;
    public int[] address = new int[4]; // sqr, row, col, isJoker?
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StatusUp()
    {
        status = status + 1;
    }

    public void RotateToLeft()
    {
        if (rotationState == -270)
        {
            rotationState = 0;
        }
        else
        {
            rotationState -= 90;
        }
    }
    public void RotateToRight()
    {
        if (rotationState == 270)
        {
            rotationState = 0;
        }
        else
        {
            rotationState += 90;
        }
    }
}
