﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingPanelScript : MonoBehaviour
{
    public GameObject rowExample;
    public Image altRankingModeBtnImage;
    public Sprite classicSprite, advancedSprite;
    public Text titleRankingPanel;
    private char activeRankMode;
    private const char ADVANCED_RANK_MODE = 'a', CLASSIC_RANK_MODE = 'c';
    private GameObject[] createdRows = new GameObject[10];

    string GetPrefix(char mode)
    {
        string prefix = "_quadlog_";
        if (mode == ADVANCED_RANK_MODE)
            prefix += "x";
        else
            prefix += "classic";
        return prefix;
    }
    void LoadData()
    {
        //Debug.Log("Loading '" + activeRankMode.ToString() + "' ranking...");
        string prefix = this.GetPrefix(this.activeRankMode);
        if (this.activeRankMode == ADVANCED_RANK_MODE)
        {
            titleRankingPanel.text = "M O D O   A V A N Ç A D O";
            this.altRankingModeBtnImage.sprite = this.classicSprite;
        }
        else
        {
            titleRankingPanel.text = "M O D O   C L A S S I C O";
            this.altRankingModeBtnImage.sprite = this.advancedSprite;
        }
        // Cria as 10 posições para amostragem da pontuação máxima
        for (int i = 0; i < 10; i += 1)
        {
            GameObject newRow = Instantiate(rowExample, rowExample.transform.parent);
            newRow.name = "pos_" + i.ToString() + "_row";
            if (!PlayerPrefs.HasKey(prefix + "_rank_" + i.ToString() + "_points") || PlayerPrefs.GetInt(prefix + "_rank_" + i.ToString() + "_points") <= 0)
            {
                newRow.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString() + "º";
                newRow.transform.GetChild(1).GetComponent<Text>().text = "<vazio>";
                newRow.transform.GetChild(2).GetComponent<Text>().text = "--/--/----";
                newRow.transform.GetChild(3).GetComponent<Text>().text = "--";
            }
            else
            {
                newRow.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString() + "º";
                newRow.transform.GetChild(1).GetComponent<Text>().text = PlayerPrefs.GetString(prefix + "_rank_" + i.ToString() + "_nickname");
                newRow.transform.GetChild(2).GetComponent<Text>().text = PlayerPrefs.GetString(prefix + "_rank_" + i.ToString() + "_date");
                newRow.transform.GetChild(3).GetComponent<Text>().text = PlayerPrefs.GetInt(prefix + "_rank_" + i.ToString() + "_points").ToString();
            }
            // TODO: Terminar e corrigir valores estaparfúdios gerados....
            newRow.GetComponent<RectTransform>().anchoredPosition = new Vector2(20f, -60 * (i + 1));
            newRow.SetActive(true);
            createdRows[i] = newRow;
        }
    }

    public void ToggleRankingPanel()
    {
        //Debug.Log("Opening panel...");
        if (!this.gameObject.activeSelf)
        {
            if (GameRules.isAdvanced)
            {
                this.activeRankMode = ADVANCED_RANK_MODE;
            }
            else
            {
                this.activeRankMode = CLASSIC_RANK_MODE;
            }
            LoadData();
        }
        else
        {
            CleanTable();
        }
        this.gameObject.SetActive(!this.gameObject.activeSelf);
    }
    public void AlterRankingMode()
    {
        CleanTable();
        if (this.activeRankMode == ADVANCED_RANK_MODE)
        {
            this.activeRankMode = CLASSIC_RANK_MODE;
            this.altRankingModeBtnImage.sprite = this.advancedSprite;
        }
        else
        {
            this.activeRankMode = ADVANCED_RANK_MODE;
            this.altRankingModeBtnImage.sprite = this.classicSprite;
        }
        LoadData();
    }
    private void CleanTable()
    {
        foreach (GameObject row in createdRows)
        {
            if (row != null)
                Destroy(row);
        }
    }
    /**
     * Código criado com base no código do "Mancala Brasil"
     * Ver mais em: xxx/mancala-brasil
     * 
     * Verifica se o jogador obteve pontuação equivalente a uma posição no rank.
     * Se for o caso, retorna a posição no rank ou -1 caso o jogador não tenha tido pontuação
     * suficiente para estar entre os 10 primeiros.
     */
    private int HasRankLevel(char mode, int points)
    {
        int rIdx = 0;
        bool hasRankLevel = false;
        string prefix = GetPrefix(mode);
        do
        {
            int p = PlayerPrefs.GetInt(prefix + "_rank_" + (rIdx + 1).ToString() + "_points");
            if (p < 1 || points > p)
            {
                hasRankLevel = true;
            }
            rIdx += 1;
        }
        while (hasRankLevel == false && rIdx < 10);
        if (!hasRankLevel)
        {
            return -1;
        }
        return rIdx;
    }
    public int IsAbleToClassicGameModeLevel(int points)
    {
        return HasRankLevel(CLASSIC_RANK_MODE, points);
    }
    public int IsAbleToAdvancedGameModeLevel(int points)
    {
        return HasRankLevel(ADVANCED_RANK_MODE, points);
    }
    private void RegisterRank(char mode, int pos, string playerName, int playerPoints)
    {
        List<int> posPrevPlayerPoints = new List<int>();
        List<string> posPrevPlayerName = new List<string>();
        List<string> posPrevPlayerRecordDate = new List<string>();
        bool ok = false;
        string prefix = GetPrefix(mode);
        for (int posIdx = pos; posIdx <= 10; posIdx += 1)
        {
            if (PlayerPrefs.HasKey(prefix + "_rank_" + posIdx.ToString() + "_points") && PlayerPrefs.HasKey(prefix + "_rank_" + posIdx.ToString() + "_nickname"))
            {
                posPrevPlayerPoints.Add(PlayerPrefs.GetInt(prefix + "_rank_" + posIdx.ToString() + "_points"));
                posPrevPlayerName.Add(PlayerPrefs.GetString(prefix + "_rank_" + posIdx.ToString() + "_nickname"));
                posPrevPlayerRecordDate.Add(PlayerPrefs.GetString(prefix + "_rank_" + posIdx.ToString() + "_date"));
            }
            else
            {
                posPrevPlayerName.Add(null);
                posPrevPlayerPoints.Add(-1);
            }
            if (!ok)
            {
                PlayerPrefs.SetInt(prefix + "_rank_" + posIdx.ToString() + "_points", playerPoints);
                PlayerPrefs.SetString(prefix + "_rank_" + posIdx.ToString() + "_nickname", playerName);
                PlayerPrefs.SetString(prefix + "_rank_" + posIdx.ToString() + "_date", System.DateTime.Now.Date.ToShortDateString());
                ok = true;
                continue;
            }
            if (posPrevPlayerName[0] != null && posPrevPlayerPoints[0] > 1)
            {
                PlayerPrefs.SetString(prefix + "_rank_" + posIdx.ToString() + "_nickname", posPrevPlayerName[0]);
                posPrevPlayerName.RemoveAt(0);
                PlayerPrefs.SetInt(prefix + "_rank_" + posIdx.ToString() + "_points", posPrevPlayerPoints[0]);
                posPrevPlayerPoints.RemoveAt(0);
                PlayerPrefs.SetString(prefix + "_rank_" + posIdx.ToString() + "_date", posPrevPlayerRecordDate[0]);
                posPrevPlayerRecordDate.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }
    public void RegisterInClassicGameModeRank(int pos, string playerName, int playerPoints)
    {
        RegisterRank(CLASSIC_RANK_MODE, pos, playerName, playerPoints);
    }
    public void RegisterInAdvancedGameModeRank(int pos, string playerName, int playerPoints)
    {
        RegisterRank(ADVANCED_RANK_MODE, pos, playerName, playerPoints);
    }
}
