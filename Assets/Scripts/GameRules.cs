﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRules : MonoBehaviour
{
    public static int pTurn, quadranteAtivo, sentidoGiro;
    public static int[] playersChars = new int[] { -1, -1 };
    public static int[] playersPreJokerRemainingTurns = new int[] { 0, 0 };
    public static string[] playersNames = new string[] { null, null };
    public static float playedTurns;
    public static bool isReady, isOver, isAdvanced, is2ndPlayerAuto;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
