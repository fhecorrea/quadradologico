﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public int buttonNumber;
    public GameObject controlador;
    // Start is called before the first frame update
    void Start()
    {
        controlador = GameObject.Find("Controlador");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void MarkBolinha()
    {
        controlador.SendMessage("Mark", buttonNumber);
    }
}
