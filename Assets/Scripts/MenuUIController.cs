﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuUIController : GameSettings
{
    /**
    TODO: Testar fluxo, remover integração com os personagens e testar nova forma de verificar andamento da partida
    */
    public Button matchConfigContinueBtn, playerConfigContinueBtn;
    public Button[] charSelectionBtns = new Button[4], gameModeBtns = new Button[2], playersNumBtns = new Button[2];
    public GameObject credits, generalMatchPanel, loadingScreen, mainMenu, playerConfigPanel, selectedGameModeIndicator, selectedNumPlayersIndicator, selectedCharIndicator, rankingPanel, regras;
    public InputField playerNameInput;
    public string CenaJogar;
    public Text gameStatusText, loadingTipText, mc_gameModeResumeText, pc_matchConfigResumeText, mc_playersNumResumeText, pc_resumeText, pc_TitleText;
    private int pass = 0, pl = 0;
    int playerTurn = 1, selectedCharIdx = -1, selectedGameModeIdx = -1, selectedPlayerNum = -1;
    string currentPlayerName = "",
           gameModeResumeTextPlaceholder1 = "<nenhum modo de jogo selecionado>",
           numPlayersResumeTextPlaceholder1 = "<número de jogadores não selecionado>",
           playerConfigResumeTextPlaceholder = "<nenhum nome e personagem selecionado>";
    //string defaultGameModeResumeTextPlaceholder1 = "Configuraçoes da partida:\n\t- Modo de jogo: xxx.\n\t- Num. jogadores: 99 jogadores.";
    void Start()
    {
        //ReloadAudioState();
        this.isGeneralSettings = true;
        this.gObjToHideOnSettingsPanelAppearing = this.mainMenu;
    }
    void Update()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            gameStatusText.text = "Offline";
            gameStatusText.color = Color.red;
        }
        else
        {
            gameStatusText.text = "Online";
            gameStatusText.color = Color.yellow;
        }

        if (generalMatchPanel.activeSelf)
            matchConfigContinueBtn.interactable = (this.selectedGameModeIdx != -1 && this.selectedPlayerNum != -1);
        if (playerConfigPanel.activeSelf)
            playerConfigContinueBtn.interactable = (this.selectedCharIdx > -1 && this.currentPlayerName.Length > 0);
        /*
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            //Debug.Log(mainMenu.activeSelf);
            //Debug.Log(modo.activeSelf);
            if (credits.activeSelf || regras.activeSelf)
            {
                HideAllPanels();
            }
            else if (playerNamePanel.activeSelf)
            {
                NamePlayer();
            }
            else if (charSelectionPanel.activeSelf)
            {
                SaveSelectedChar();
            }
        }
        if (charSelectionPanel.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
            {
                SelectDeltaChar();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
            {
                SelectInfinitChar();
            }
            if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
            {
                SelectPiChar();
            }
            if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
            {
                SelectVectorChar();
            }
        }

        if (playersNumberPanel.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
            {
                SelectOnePlayer();
            }
            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
            {
                SelectTwoPlayers();
            }
        }
        */
        //CheckAudioState();
        ReloadAudioState();
    }

    void SelectPlayersNum(int num)
    {
        if (this.selectedPlayerNum == num)
        {
            this.selectedPlayerNum = -1;
            this.selectedNumPlayersIndicator.SetActive(false);
            this.mc_playersNumResumeText.color = Color.red;
            this.mc_playersNumResumeText.text = this.numPlayersResumeTextPlaceholder1;
            return;
        }
        this.selectedNumPlayersIndicator.transform.position = this.playersNumBtns[num - 1].transform.position;
        this.selectedNumPlayersIndicator.SetActive(true);
        this.selectedPlayerNum = num;
        this.mc_playersNumResumeText.text = "Foi escolhido jogar " + (num == 1 ? "contra o computador" : "contra um amigo") + ".";
        this.mc_playersNumResumeText.color = Color.green;
    }
    void SelectGameMode(int num)
    {
        if (this.selectedGameModeIdx == num)
        {
            this.selectedGameModeIdx = -1;
            this.selectedGameModeIndicator.SetActive(false);
            this.mc_gameModeResumeText.color = Color.red;
            this.mc_gameModeResumeText.text = this.gameModeResumeTextPlaceholder1;
            return;
        }
        this.selectedGameModeIndicator.transform.position = this.gameModeBtns[num - 1].transform.position;
        this.selectedGameModeIndicator.SetActive(true);
        this.selectedGameModeIdx = num;
        this.mc_gameModeResumeText.text = "Foi escolhido o modo " + (num == 1 ? "\"CLÁSSICO\"" : "\"AVANÇADO\"") + ".";
        if (num == 1)
            this.mc_gameModeResumeText.color = Color.white;
        else
            this.mc_gameModeResumeText.color = new Color(1f, 0.49f, 0.00f, 1f);
    }
    void SelectChar(int num)
    {
        if (this.selectedCharIdx == num)
        {
            this.selectedCharIdx = -1;
            this.selectedCharIndicator.SetActive(false);
            this.pc_resumeText.text = this.playerConfigResumeTextPlaceholder;
            this.pc_resumeText.color = Color.gray;
            return;
        }
        this.selectedCharIndicator.transform.position = this.charSelectionBtns[num - 1].transform.position;
        this.selectedCharIndicator.SetActive(true);
        this.selectedCharIdx = num;
        SetPlayerResumeText();
    }
    void SetPlayerResumeText()
    {
        string chrText;
        Color chrColor;
        switch (this.selectedCharIdx)
        {
            case 1:
                chrColor = new Color(0.90f, 0.17f, 0.31f, 1f);
                chrText = "personagem: Delta";
                break;
            case 2:
                chrColor = Color.cyan;
                chrText = "personagem: Infinit";
                break;
            case 3:
                chrColor = Color.red;
                chrText = "personagem: Pi";
                break;
            case 4:
                chrColor = Color.green;
                chrText = "personagem: Vector";
                break;
            default:
                chrColor = Color.white;
                chrText = "<nenhuma personagem>";
                break;
        }
        this.pc_resumeText.text = (this.currentPlayerName == "" ? "Jogador <sem nome>" : "Nome: \"" + this.currentPlayerName + "\"") + ", " + chrText;
        this.pc_resumeText.color = chrColor;
    }

    public void Play()
    {
        mainMenu.SetActive(false);
        generalMatchPanel.SetActive(true);
    }
    public void Back()
    {
        // Se estiver na primeira configuração da partida
        if (generalMatchPanel.activeSelf)
        {
            selectedGameModeIdx = selectedPlayerNum = -1;
            selectedGameModeIndicator.SetActive(false);
            selectedNumPlayersIndicator.SetActive(false);
            generalMatchPanel.SetActive(false);
            HideAllPanels();
        }
        else if (playerConfigPanel.activeSelf)
        {
            playerConfigPanel.SetActive(false);
            if (playerTurn == 1)
            {
                currentPlayerName = "";
                this.playerNameInput.text = "";
                selectedCharIdx = -1;
            }
            else
            {
                playerTurn = 1;
                currentPlayerName = GameRules.playersNames[playerTurn - 1];
                this.playerNameInput.text = currentPlayerName;
                selectedCharIdx = GameRules.playersChars[playerTurn - 1];
                pc_TitleText.text = "Configuração do " + playerTurn + "º jogador";
            }
            generalMatchPanel.SetActive(true);
        }
    }
    public void Next()
    {
        if (generalMatchPanel.activeSelf)
        {
            generalMatchPanel.SetActive(false);
            pc_matchConfigResumeText.text = "Configurações da partida:\n\t- Modo de jogo: " + (this.selectedGameModeIdx == 1 ? "Clássico" : "Avançado") + ".\n\t- Qtd. de jogadores: " + (this.selectedPlayerNum > 0 ? this.selectedPlayerNum.ToString() + " jogador" : "nenhum jogador") + (this.selectedPlayerNum > 1 ? "es" : "") + ".";
            playerConfigPanel.SetActive(true);
        }
        else if (playerConfigPanel.activeSelf)
        {
            playerConfigPanel.SetActive(false);
            GameRules.playersNames[playerTurn - 1] = this.currentPlayerName;
            GameRules.playersChars[playerTurn - 1] = this.selectedCharIdx;
            if (playerTurn == this.selectedPlayerNum)
            {
                GameRules.isAdvanced = this.selectedGameModeIdx == 2;
                GameRules.is2ndPlayerAuto = this.selectedPlayerNum == 1;
                if (GameRules.is2ndPlayerAuto)
                {
                    Debug.Log("before sorting char idx. for #CPU player. Char idx. of #" + pl.ToString() + " player: " + GameRules.playersChars[0].ToString());
                    do
                    {
                        GameRules.playersChars[1] = Random.Range(0, 4);
                        Debug.Log(GameRules.playersChars[1].ToString() + " sorted. Is equal to #1 player? " + (GameRules.playersChars[1] == GameRules.playersChars[0]).ToString());
                    }
                    while (GameRules.playersChars[1] == GameRules.playersChars[0]);
                }
                ShowLoadingScreen();
                StartCoroutine(StartGame());
            }
            else
            {
                playerTurn += 1;
                pc_TitleText.text = "Configuração do " + playerTurn + "º jogador";
                this.currentPlayerName = "";
                this.playerNameInput.text = "";
                this.selectedCharIdx = -1;
                this.selectedCharIndicator.SetActive(false);
                playerConfigPanel.SetActive(true);
                // Autofocus no campo de preenchimento do nome do jogador
                EventSystem.current.SetSelectedGameObject(playerNameInput.gameObject, null);
                playerNameInput.OnPointerClick(new PointerEventData(EventSystem.current));
            }
        }
    }
    /*
    public void Jogar()
    {
        if (this.pass == 0)
        {
            PassToNextPass();
        }
        else
        {
            if (GameRules.is2ndPlayerAuto)
            {
                continue.text = "Jogar";
                GameRules.playersNames[1] = "#CPU";
                Debug.Log("before sorting char idx. for #CPU player. Char idx. of #" + pl.ToString() + " player: " + GameRules.playersChars[0].ToString());
                do
                {
                    GameRules.playersChars[1] = Random.Range(0, 4);
                    Debug.Log(GameRules.playersChars[1].ToString() + " sorted. Is equal to #1 player? " + (GameRules.playersChars[1] == GameRules.playersChars[0]).ToString());
                }
                while (GameRules.playersChars[1] == GameRules.playersChars[0]);
            }
            Debug.Log("GameRules.playersChars[0] before game starting: " + GameRules.playersChars[0].ToString());
            Debug.Log("GameRules.playersChars[1] before game starting: " + GameRules.playersChars[1].ToString());
            HideAllPanels();
            mainMenu.SetActive(false);
            ShowLoadingScreen();
            StartCoroutine(StartGame());
        }
    }
    */


    public void Regras()
    {
        mainMenu.SetActive(false);
        regras.SetActive(true);
    }
    public void Creditos()
    {
        mainMenu.SetActive(false);
        credits.SetActive(true);
    }

    public void SelectClassicMode()
    {
        SelectGameMode(1);
    }
    public void SelectAdvancedMode()
    {
        SelectGameMode(2);
    }
    public void SelectOnePlayer()
    {
        SelectPlayersNum(1);
    }
    public void SelectTwoPlayers()
    {
        SelectPlayersNum(2);
    }
    public void SelectDeltaChar()
    {
        this.SelectChar(1);
    }
    public void SelectInfinitChar()
    {
        this.SelectChar(2);
    }
    public void SelectPiChar()
    {
        this.SelectChar(3);
    }
    public void SelectVectorChar()
    {
        this.SelectChar(4);
    }

    /*
    private void SelectIndexChar(int index)
    {
        if (index == selectedCharIdx)
        {
            selectedCharIndicator.SetActive(false);
            selectedCharIdx = -1;
            return;
        }
        //GameObject aftSelObj = GameObject.Find("Character (" + GameRules.playersChars[pl].ToString() + ")");
        //GameObject newSelCharBtn = GameObject.FindWithTag("Selected");
        avCharacters[index].SetActive(true);
        GameRules.playersChars[pl] = index;
        Debug.Log("pl==" + pl.ToString());
    }

    public void SaveSelectedChar()
    {
        if (GameRules.playersChars[pl] == -1)
        {
            return;
        }
        charSelectionPanel.SetActive(false);
        avCharacters[GameRules.playersChars[pl]].SetActive(false);
        p += 1;
        pl += 1;
        RefreshPass();
    }
    */

    void ShowLoadingScreen()
    {
        string[] tips = new string[] {
            "Você sabe quantos são 3³ (três elevado a três)?",
            "Se x = 1, quanto é x² - x = 3 ?",
            "Você sabe o que é a Fórmula de Baskhara?",
            "Respeitar o professor é a chave do sucesso!",
            "Dica: Tente estudar a jogada de seu adversario.",
            "Dica: Leia o jogo."
        };
        int sortIdx = (int)System.Math.Floor(Random.Range(0, tips.Length - 1) * 1f);
        loadingTipText.text = tips[sortIdx];
        loadingScreen.SetActive(true);
    }

    /*
    public void Classico()
    {
        GameRules.isAdvanced = false;
        GameRules.sentidoGiro = 1;
        PassToNextPass();
    }
    public void Avancado()
    {
        GameRules.isAdvanced = true;
        PassToNextPass();
    }
    public void RefreshPass()
    {
        SetCurrentPass(this.pass);
    }
    public void PassToNextPass()
    {
        SetCurrentPass(++this.pass);
    }
    public void BackToPreviousPass()
    {
        SetCurrentPass(--this.pass);
    }
    public void BackToAntPreviousPass()
    {
        charSelectionPanel.SetActive(false);
        SetCurrentPass(this.pass - 1);
    }
    public void SetCurrentPass(int currentPass)
    {
        this.pass = currentPass;
        //Debug.Log(currentPass.ToString());
        switch (currentPass)
        {
            case 1:
                mainMenu.SetActive(false);
                playersNumberPanel.SetActive(false);
                gameModePanel.SetActive(true);
                break;
            case 2:
                gameModePanel.SetActive(false);
                playerNamePanel.SetActive(false);
                playersNumberPanel.SetActive(true);
                break;
            case 3:
                playersNumberPanel.SetActive(false);
                //if (charSelectionPanel.activeSelf)
                //{
                charSelectionPanel.SetActive(false);
                //}
                titlePlayerNamePanel.text = "Entre com o nome do jogador #" + p.ToString();
                playerNamePanel.SetActive(true);
                // Autofocus no campo de preenchimento do nome do jogador
                EventSystem.current.SetSelectedGameObject(playerNameInput.gameObject, null);
                playerNameInput.OnPointerClick(new PointerEventData(EventSystem.current));
                break;
            case 4:
                playerNamePanel.SetActive(false);
                Debug.Log("p==" + p);
                if (pl == 1)
                {
                    charSelectionBtns[GameRules.playersChars[0]].interactable = false;
                }
                if (GameRules.is2ndPlayerAuto)
                {
                    this.pass += 1;
                    p += 1;
                }
                else if (p == 2)
                {
                    continueAfterCharSelectionBtnText.text = "Jogar";
                    this.pass += 1;
                }
                else
                {
                    continueAfterCharSelectionBtnText.text = "Continuar";
                    // Ao atualizar, carrega a página de nomeação do jogador
                    this.pass -= 1;
                }
                charSelectionPanel.SetActive(true);
                break;
            default:
                Jogar();
                break;
        }
    }
    */

    public void Sair()
    {
        Application.Quit();
    }
    public void CancelGameStarting()
    {
        //p = 1;
        //pl = 0;
        //SetCurrentPass(0);
        GameRules.playersChars = new int[] { -1, -1 };
        GameRules.playersNames = new string[] { null, null };
        HideAllPanels();
    }
    public void HideAllPanels()
    {
        mainMenu.SetActive(true);
        regras.SetActive(false);
        credits.SetActive(false);
        generalMatchPanel.SetActive(false);
        playerConfigPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    public void NamePlayer(string name)
    {
        this.currentPlayerName = name.Trim();
        SetPlayerResumeText();
    }
    /*
    public void SkipNaming()
    {
        playerNameInput.text = null;
        PassToNextPass();
    }
    public void NamePlayer()
    {
        if (playerNameInput.text != null && playerNameInput.text.Length > 0)
        {
            GameRules.playersNames[p - 1] = playerNameInput.text.Trim();
            playerNameInput.text = null;
            PassToNextPass();
        }
        else
        {
            GameRules.playersNames[p - 1] = "Anônimo 👻";
            this.SkipNaming();
        }
    }
    */

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(CenaJogar);
    }

}
