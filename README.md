# Quadrado Lógico - 1.1.0

Este projeto é um jogo criado por Prof. Edmilson Melo ([@natrilhadojogo](https://instagram.com/natrilhadojogo/), no Instagram) para o curso de Programação de Jogos Digitais e desenvolvido pelo professor Suami Abdalla ([@prof_abdalla](https://instagram.com/prof_abdalla/), no Instagram).

O que inicialmente era um jogo para Windows, passou a ser direcionado para os dispositivos móveis e ganhou outros recursos também pensados pelo professor Edmilson, só que desta fez postos em prática por mim.

Apesar deste projeto estar em meu repositório, sendo assim, a guarda do projeto estar em minhas mãos, na verdade este projeto tem as mais variadas contribuções, como algumas já aqui citadas e também a parte da arte, cenários, personagens e alguns itens de jogo que ficaram nas mãos da designer e desenhista Júlia de Oliveira Rodrigues ([@jubsartes](https://instagram.com/jubsartes/)).
